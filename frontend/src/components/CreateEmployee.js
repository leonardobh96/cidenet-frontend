import axios from "axios";
import React, { useState, useEffect } from "react";
import { Navigate, useNavigate } from "react-router-dom";
import Select from "react-select";

const endpoint = "http://localhost:8000/api";

const CreateEmployee = () => {
  const [first_name, setFirst_name] = useState("");
  const [other_name, setOther_name] = useState("");
  const [surname, setSurname] = useState("");
  const [second_surname, setSecond_surname] = useState("");
  const [date_of_admission, setDate_of_admission] = useState("");
  const [types, setTypes] = useState([]);
  const [countries, setCountries] = useState([]);
  const [areas, setAreas] = useState([]);
  const [dni_number, setDni_number] = useState("");
  const [dateMax, setDateMax] = useState("");
  const [dateMin, setDateMin] = useState("");
  const [dniType, setDniType] = useState("");
  const [selectedCountry, setSelectedCountry] = useState("");
  const [selectedArea, setSelectedArea] = useState("");

  const navigate = useNavigate();
  useEffect(() => {
    getFields();
  }, []);

  const getFields = async () => {
    await axios
      .get(`${endpoint}/countries`)
      .then((response) => {
        setCountries(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
    await axios
      .get(`${endpoint}/types`)
      .then((response) => {
        setTypes(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
    await axios
      .get(`${endpoint}/areas`)
      .then((response) => {
        setAreas(response.data);
      })
      .catch((error) => {
        console.log(error);
      });
    let fecha = new Date();
    const day =fecha.getDay() < 10 ? `0${fecha.getDay() - 2}` : fecha.getDay() - 2;
    let month =fecha.getMonth() < 10 ? `0${fecha.getMonth()}` : fecha.getMonth();
    const dateMin = `${fecha.getFullYear()}-${month}-${day}`;
    setDateMin(dateMin);
    console.log("dateMin", dateMin);
    month = fecha.getMonth() < 10 ? `0${fecha.getMonth() + 1}` : fecha.getMonth() + 1;
    const dateMax = `${fecha.getFullYear()}-${month}-${day}`;
    setDateMax(dateMax);
    console.log("dateMax", dateMax);
  };

  const store = async (e) => {
    e.preventDefault();
    await axios.post(`${endpoint}/employee`, {
      first_name: first_name,
      other_name: other_name,
      surname: surname,
      second_surname: second_surname,
      date_of_admission: date_of_admission,
      dni_number: dni_number,
      dniType_id: dniType,
      country_id: selectedCountry,
      area_id: selectedArea,
      state: true,
    });
    navigate("/");
  };
  return (
    <div className="container">
      <h3 className="m-4">Registrar Empleado</h3>
      <form onSubmit={store}>
        <div className="row">
          <div className="mb-3 col-sm-3">
            <label className="form-label">Primer nombre</label>
            <input
              value={first_name}
              onChange={(e) => setFirst_name(e.target.value)}
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3 col-sm-3">
            <label className="form-label">Otros nombres</label>
            <input
              value={other_name}
              onChange={(e) => setOther_name(e.target.value)}
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3 col-sm-3">
            <label className="form-label">Primer apellido</label>
            <input
              value={surname}
              onChange={(e) => setSurname(e.target.value)}
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3 col-sm-3">
            <label className="form-label">Segundo apellido</label>
            <input
              value={second_surname}
              onChange={(e) => setSecond_surname(e.target.value)}
              type="text"
              className="form-control"
            />
          </div>
        </div>
        <div className="row">
          <div className="mb-3 col-sm-3">
            <label className="form-label">Fecha de ingreso</label>
            <input
              value={date_of_admission}
              onChange={(e) => setDate_of_admission(e.target.value)}
              type="date"
              className="form-control"
              min={dateMin}
              max={dateMax}
            />
          </div>
          <div className="mb-3 col-sm-3">
            <label className="form-label">Tipo de identificación</label>
            <Select
              defaultValue={types[0]}
              options={types.map((type) => ({
                label: type.name,
                value: type.id,
              }))}
              onChange={({ value }) => setDniType(value)}
            />
          </div>
          <div className="mb-3 col-sm-2">
            <label className="form-label">Número de identificación</label>
            <input
              value={dni_number}
              onChange={(e) => setDni_number(e.target.value)}
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3 col-sm-2">
            <label className="form-label">Pais del empleo</label>
            <Select
              options={countries.map((country) => ({
                label: country.name,
                value: country.id,
              }))}
              onChange={({ value }) => setSelectedCountry(value)}
            />
          </div>
          <div className="mb-3 col-sm-2">
            <label className="form-label">Área de trabajo</label>
            <Select
              options={areas.map((area) => ({
                label: area.name,
                value: area.id,
              }))}
              onChange={({ value }) => setSelectedArea(value)}
            />
          </div>
        </div>
        <button type="submit" className="btn btn-success">
          Registrar
        </button>
      </form>
    </div>
  );
};

export default CreateEmployee;
