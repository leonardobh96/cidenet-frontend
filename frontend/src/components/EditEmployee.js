import axios from "axios";
import React, { useState, useEffect } from "react";
import { useNavigate, useParams } from "react-router-dom";
import Select from "react-select";

const endpoint = "http://localhost:8000/api";

const EditEmployee = () => {
  const [first_name, setFirst_name] = useState("");
  const [other_name, setOther_name] = useState("");
  const [surname, setSurname] = useState("");
  const [second_surname, setSecond_surname] = useState("");
  const [dni_number, setDni_number] = useState("");
  const [dniType, setDniType] = useState("");
  const [selectedCountry, setSelectedCountry] = useState("");
  const [selectedArea, setSelectedArea] = useState("");
  const [types, setTypes] = useState([]);
  const [countries, setCountries] = useState([]);
  const [areas, setAreas] = useState([]);
  const { id } = useParams();
  const navigate = useNavigate();

  const update = async (e) => {
    e.preventDefault();
    await axios.put(`${endpoint}/employee/${id}`, {
      first_name: first_name,
      other_name: other_name,
      surname: surname,
      second_surname: second_surname,
      dni_number: dni_number,
      dniType_id: dniType,
      country_id: selectedCountry,
      area_id: selectedArea,
    });
    navigate("/");
  };

  useEffect(() => {
    const getEmployee = async () => {
      const response = await axios.get(`${endpoint}/employee/${id}`);
      console.log(response.data)
      setFirst_name(response.data.first_name);
      setOther_name(response.data.other_name);
      setSurname(response.data.surname);
      setSecond_surname(response.data.second_surname);
      setDni_number(response.data.dni_number);
      setDniType(response.data.dniType_id);
      setSelectedCountry(response.data.country_id);
      setSelectedArea(response.data.area_id);
    };
    const getFields = async () => {
      await axios
        .get(`${endpoint}/countries`)
        .then((response) => {
          setCountries(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
      await axios
        .get(`${endpoint}/types`)
        .then((response) => {
          setTypes(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
      await axios
        .get(`${endpoint}/areas`)
        .then((response) => {
          setAreas(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    };
    getEmployee();
    getFields();
    
  }, []);

  return (
    <div className="container">
      <h3 className="m-4">Registrar Empleado</h3>
      <form onSubmit={update}>
        <div className="row">
          <div className="mb-3 col-sm-4">
            <label className="form-label">Primer nombre</label>
            <input
              value={first_name}
              onChange={(e) => setFirst_name(e.target.value)}
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3 col-sm-4">
            <label className="form-label">Otros nombres</label>
            <input
              value={other_name}
              onChange={(e) => setOther_name(e.target.value)}
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3 col-sm-4">
            <label className="form-label">Primer apellido</label>
            <input
              value={surname}
              onChange={(e) => setSurname(e.target.value)}
              type="text"
              className="form-control"
            />
          </div>
        </div>
        <div className="row">
          <div className="mb-3 col-sm-3">
            <label className="form-label">Segundo apellido</label>
            <input
              value={second_surname}
              onChange={(e) => setSecond_surname(e.target.value)}
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3 col-sm-3">
            <label className="form-label">Tipo de identificación</label>
            <Select
              
              options={types.map((type) => ({
                label: type.name,
                value: type.id,
              }))}
              defaultValue={types[0]}
              onChange={({ value }) => setDniType(value)}
            />
          </div>
          <div className="mb-3 col-sm-2">
            <label className="form-label">Número de identificación</label>
            <input
              value={dni_number}
              onChange={(e) => setDni_number(e.target.value)}
              type="text"
              className="form-control"
            />
          </div>
          <div className="mb-3 col-sm-2">
            <label className="form-label">Pais del empleo</label>
            <Select
              options={countries.map((country) => ({
                label: country.name,
                value: country.id,
              }))}
              onChange={({ value }) => setSelectedCountry(value)}
            />
          </div>
          <div className="mb-3 col-sm-2">
            <label className="form-label">Área de trabajo</label>
            <Select
              options={areas.map((area) => ({
                label: area.name,
                value: area.id,
              }))}
              onChange={({ value }) => setSelectedArea(value)}
            />
          </div>
        </div>
        <button type="submit" className="btn btn-success">
          Actualizar
        </button>
      </form>
    </div>
  );
};

export default EditEmployee;
