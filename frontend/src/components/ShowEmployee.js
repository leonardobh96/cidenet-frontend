import React, {useEffect, useState} from 'react'
import axios from 'axios'

import {Link} from 'react-router-dom'

const endpoint = 'http://localhost:8000/api'
const ShowEmployee = () => {

    const [ employees, setEmployees ] = useState( [] );

    useEffect ( ()=> {
        getAllEmployees();
    }, []);

    const getAllEmployees = async () =>{
        const response = await axios.get(`${endpoint}/employees`);
        setEmployees(response.data);
    }

    const deleteEmployee = async (employee) => {
        await axios.delete(`${endpoint}/employee/${employee}`);
        getAllEmployees();
    }
  return (
    <div className='p-1 m-4'>
        <div className='container text-center'>
            <Link to="/create" className="btn btn-primary btn-md mt-2 mb-2 text-white w-25">Registrar empleado</Link>
        </div>

        <table className='table table-striped'>
            <thead className='bg-dark text-white'>
                <tr>
                    <th>Nombres</th>
                    <th>Primer apellido</th>
                    <th>Segundo apellido</th>
                    <th>Correo</th>
                    <th>Fecha de ingreso</th>
                    <th>Estado</th>
                    <th>Número de identificación</th>
                    <th>Pais del empleo</th>
                    <th>Área de trabajo</th>
                    <th>Fecha de ingreso</th>
                    <th>Acciones</th>
                </tr>
            </thead>
            <tbody>
                { employees.map( (employee) => (
                    <tr key={employee.id}>
                        <td>{employee.first_name} {employee.other_name}</td>
                        <td>{employee.surname}</td>
                        <td>{employee.second_surname} </td>
                        <td>{employee.email} </td>
                        <td>{employee.date_of_admission} </td>
                        <td>{employee.state} </td>
                        <td>{employee.dni_number} </td>
                        <td>{employee.country.name} </td>
                        <td>{employee.area.name} </td>
                        <td>{employee.created_at} </td>
                        <td>
                            <Link to={`/edit/${employee.id}`} className="btn btn-warning">Editar</Link>
                            <button onClick={ ()=>deleteEmployee(employee.id) } className="btn btn-danger" >Eliminar</button>
                        </td>
                    </tr>
                ))}
            </tbody>
        </table>
        
    </div>
  )
}

export default ShowEmployee